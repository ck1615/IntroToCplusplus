Introduction to C++: Lesson 6
=============================

Topics
------

1. [Dynamic Arrays](#61-dynamic-arrays),
2. [Working With Files](#62-working-with-files)

6.1 Dynamic Arrays
------------------

While we can do a lot with static arrays, it's quite useful to be able to
define the size of an array only when the code is executed.

C++ provides the `new` and `delete` operators for handling dynamic memory
allocation and deallocation. To allocate a variable-sized array we use the
`new` keyword followed by the type of the array and its size in square
brackets. An array allocated with new stays allocated until the program ends
unless it is explicitly deleted. This is in contrast to other variables: the
memory associated with static variables is released when that variable goes out
of scope (i.e. at the block of code in which it is declared). It is good
practice to delete arrays as needed, if only to maximise the available amount
of memory! Deleting an array allocated using `new` is performed using
`delete[]`. The square brackets tell delete to deallocate the entire array
rather than just the first element of the array.

Examples showing how dynamic arrays can be used are given in the directory
`6.1_Dynamic_Arrays`. A basic example is given in `dynamic1.cpp`:

```c++
// Example showing how to use dynamic arrays.
#include <iostream>
using namespace std;

int main() {
    int n, *arr;

    cout << "Enter an integer: ";
    cin >> n;

    arr = new int[n];
    // Here we use the "new" command to allocate an array of the input size.

    for (int i = 0; i < n; ++i) {
        arr[i] = 2 * i + 1;
        cout << arr[i] << endl;
    }
    delete[] arr;
    // Now we deallocate the array now we are finished with it using "delete".

    return 0;
}
```

### [Exercises](../exercises/README.md#61-dynamic-arrays)

Please complete the exercises listed in
[section ](../exercises/README.md#61-dynamic-arrays) of the file
`exercises/README.md`.

6.2 Working With Files
----------------------

It's often useful to be able to read from, and write to various files. The
first example is in `6.2_Files/files1.cpp` in which a file is opened and
each line in turn is read to a variable and output to stdout.
```c++
// Example showing how to read a file.
#include <iostream>
#include <fstream>
// fstream contains functions to read and write files.
#include <string>

using namespace std;

int main() {

    string input_line;

    ifstream infile;
    // ifstream is used to open a file for reading.
    infile.open("example.dat");
    // This pair of commands can be combined and shortened to
    // ifstream infile("example.dat");

    // It's good to test that the file was successfully opened before
    // trying to work with it.
    if ( ! infile.is_open() ) {
        cout << "Error opening file." << endl;
        return 1;
        // Here we return 1 to indicate an error occurred.
    }

    // Here we use the getline function to read a line from the file to a
    // string. We can use this in a while loop to iterate over every line,
    // since getline will return false when it fails to read any more lines.
    while (getline(infile, input_line)) {
        cout << input_line << endl;
    }
    infile.close();

    return 0;
}
```

We can also read numbers directly to variables using the `>>` operator
as shown in `files2.cpp`:
```c++
// Example showing how to read a file.
#include <iostream>
#include <fstream>
#include <string>

using namespace std;

int main() {

    int count;
    double x, y, x_total, y_total;

    ifstream infile("example.dat");
    if ( ! infile.is_open() ) {
        cout << "Error opening file." << endl;
        return 1;
    }

    x_total = 0.0e0;
    y_total = 0.0e0;
    count = 0;
    // This time we use >> to read numbers from the file.
    // Again we use a while loop to iterate over every line in the file.
    while ( infile >> x >> y ) {
        // Note that >> does not take account of newlines, so if we just had
        // 'infile >> x' then it would iterate over each value on each line.
        x_total += x;
        y_total += y;
        ++count;
    }
    infile.close();

    cout << "Read " << count << " values from file." << endl;
    cout << "Average x value: " << x_total / count << endl;
    cout << "Average y value: " << y_total / count << endl;

    return 0;
}
```

To write to a file, we can open the file using the `ofstream` command.
`files3.cpp` contains an example which writes the integers from 1 to 10 along
with their squares to the file `squares.dat`.

```c++
// Example showing how to write to a file.
#include <iostream>
#include <fstream>
#include <string>

using namespace std;

int main() {

    int i;

    ofstream outfile("squares.dat");
    if ( ! outfile.is_open() ) {
        cout << "Error opening file." << endl;
        return 1;
    }

    for (i = 1; i <= 10; ++i) {
        outfile << i << " " << i * i << endl;
    }

    outfile.close();

    return 0;
}
```
### [Exercises](../exercises/README.md#62-working-with-files)

Please complete the exercises listed in
[section 6.2](../exercises/README.md#62-working-with-files) of the file
`exercises/README.md`.

