// Example showing how to use dynamic arrays.
#include <iostream>
using namespace std;

int main() {
    int n, *arr;

    cout << "Enter an integer: ";
    cin >> n;

    arr = new int[n];
    // Here we use the "new" command to allocate an array of the input size.

    for (int i = 0; i < n; ++i) {
        arr[i] = 2 * i + 1;
        cout << arr[i] << endl;
    }
    delete[] arr;
    // Now we deallocate the array now we are finished with it using "delete".

    return 0;
}
