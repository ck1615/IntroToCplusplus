// An example of the use of a function that is defined in a separate file.
#include <iostream>
using namespace std;

// Note we still need the interface definition.
double quadratic_root1(double, double, double);

int main() {
    double user_a, user_b, user_c;

    cout << "Enter a, b and c for a quadratic equation (ax^2+bx+c=0): ";
    cin >> user_a;
    cin >> user_b;
    cin >> user_c;

    cout << quadratic_root1(user_a, user_b, user_c) << endl;

    return 0;
}
