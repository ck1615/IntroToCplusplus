Introduction to C++: Lesson 7
=============================

Topics
------

1. [Data Structures](#71-data-structures),
2. [Working With Larger Codes](#72-working-with-larger-codes)

7.1 Data Structures
-------------------

A data structure allows you to group related variables together into a single
object. An example of how these structures can be created and used is given
in `7.1_Data_Structures/structures1.cpp`:

```c++
// Example showing how to use data structures
#include <iostream>
#include <string>

using namespace std;

// We use the following construction to create a structure type named
// Atom, containing several different types of variables.
struct Atom {
    int atomic_number;
    double mass_number;
    double radius_pm;
    string symbol;
} ;
// Note we need to finish the definition with a ';'.

// This function will output the information stored in the structure.
// variables contained in the structure are accessed using the
// structure_name.variable_name construction.
void output_atom_info(Atom a) {
    cout << a.symbol << endl;
    cout << "Atomic number: " << a.atomic_number << endl;
    cout << "Mass number: " << a.mass_number << endl;
    cout << "Atomic radius (pm): " << a.radius_pm << endl;
}

int main() {
    // Initialize two objects of the Atom type.
    Atom hydrogen, lithium;

    // We can set the variables contained in each directly.
    hydrogen.atomic_number = 1;
    hydrogen.mass_number = 1.008;
    hydrogen.symbol = "H";
    hydrogen.radius_pm =  25;

    lithium.atomic_number = 3;
    lithium.mass_number = 6.941;
    lithium.symbol = "Li";
    lithium.radius_pm = 145;

    output_atom_info(hydrogen);
    cout << endl;
    output_atom_info(lithium);

    return 0;
}
```

### [Exercises](../exercises/README.md#71-data-structures)

Please complete the exercises listed in
[section 7.1](../exercises/README.md#71-data-structures) of the file
`exercises/README.md`.

7.2 Working With Larger Codes
-----------------------------

As codes expand in size, it's helpful to break up your code into several
files. For example, one file could contain the main function and a second file
could contain some set of related function, or a class and it's associated
functions. This makes code much easier to work with as it expands in size,
and it will also be faster to recompile when you make a change.

To see how to break up a code, let's first look again at the example in
`lesson4/4.1_Functions/functions3.cpp`:
```c++
// An example of the use of a function that returns values and has
// arguments, and is definied following main.
#include <iostream>
#include <cmath>
using namespace std;

double quadratic_root1(double, double, double);

int main() {
    double user_a, user_b, user_c;

    cout << "Enter a, b and c for a quadratic equation (ax^2+bx+c=0): ";
    cin >> user_a;
    cin >> user_b;
    cin >> user_c;

    cout << quadratic_root1(user_a, user_b, user_c) << endl;

    return 0;
}

double quadratic_root1(double a, double b, double c) {
    // Return the larger root of a quadratic equation.

    double root1;

    root1 = -b + sqrt(pow(b, 2) - 4.0 * a * c);
    root1 = root1 / (2.0 * a);
    return root1;
}
```
We can break this up into two files, as shown in the `7.2_Larger_Codes/root1`
directory. First we have the file `root.cpp` which contains the main function
with the interface to the other function defined before it:
```c++
// An example of the use of a function that is defined in a separate file.
#include <iostream>
using namespace std;

// Note we still need the interface definition.
double quadratic_root1(double, double, double);

int main() {
    double user_a, user_b, user_c;

    cout << "Enter a, b and c for a quadratic equation (ax^2+bx+c=0): ";
    cin >> user_a;
    cin >> user_b;
    cin >> user_c;

    cout << quadratic_root1(user_a, user_b, user_c) << endl;

    return 0;
}
```

And we also have a file `func.cpp` which contains the actual quadratic_root1
function:
```c++
#include <cmath>
using namespace std;

double quadratic_root1(double a, double b, double c) {
    // Return the larger root of a quadratic equation.

    double root1;

    root1 = -b + sqrt(pow(b, 2) - 4.0 * a * c);
    root1 = root1 / (2.0 * a);
    return root1;
}
```

Note in both of these files, we only need to use the `#include` line that
is relevant to functions we use in that file. For example, we only need to
use `<cmath>` in `func.cpp` since we use the `sqrt` and `pow` functions there.

To compile the executable from these, if we were not using a Makefile we would
need to do the following:
```bash
g++ -c -Wall -Wextra root.cpp -o root.o
g++ -c -Wall -Wextra func.cpp -o func.o
g++ root.o func.o -o root.x
```

However, we can simply update the sources line in the `Makefile` to read
`SOURCES = root.cpp func.cpp`, and with the rules we defined and have been
using, when we type `make` it will execute the set of commands as we have
written explicitly above.

### Header Files

If we're using many functions, it's more convenient to put all the interfaces
in a separate file also, rather than listing them all before the main function
in the main source file. To do this, we can list them all, in exactly the
same way, in a header file. This is done in the `root2` directory. First
we create a file `func.h` with the interfaces to all the functions defined
in the `func.cpp` file (just one in this case):
```c++
double quadratic_root1(double, double, double);
```
And the `root.cpp` file is now:
```c++
// An example of the use of a function that is defined in a separate file
// using a header file.
#include <iostream>
// We have this extra include line to add the header file where the interface
// is defined.
#include "func.h"
using namespace std;

int main() {
    double user_a, user_b, user_c;

    cout << "Enter a, b and c for a quadratic equation (ax^2+bx+c=0): ";
    cin >> user_a;
    cin >> user_b;
    cin >> user_c;

    cout << quadratic_root1(user_a, user_b, user_c) << endl;

    return 0;
}
```
The include statement here basically puts the contents of `func.h` in
`root.cpp` at that point.

### [Exercises](../exercises/README.md#72-working-with-larger-codes)

Please complete the exercises listed in
[section 7.2](../exercises/README.md#72-working-with-larger-codes) of the file
`exercises/README.md`.

