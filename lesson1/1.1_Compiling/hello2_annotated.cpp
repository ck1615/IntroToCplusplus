// A short C++ program to greet the world.

#include <iostream>
using namespace std;
/* C++ uses namespaces. A namespace is used to collect together a group of
 * related variables and helps avoid clashes in the names of very different
 * items. The namespace an item is in is indicated by using
 * namepace_name::item_name as we had previously with std::cout.
 * The above statement allows us to import the std namespace into the current
 * scope. This means we no longer nead to prefix items from the std namespace
 * with std::. */

int main()
{
    cout << "Hello world!" << endl;
    // Note we have dropped the std:: before cout and endl here.

    return 0;
}
