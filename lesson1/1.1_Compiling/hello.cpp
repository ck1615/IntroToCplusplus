// A short C++ program to greet the world.
#include <iostream>

int main()
{
    std::cout << "Hello world!" << std::endl;
    return 0;
}
