// Example showing how pointers can manipulate the data associated
// with a variable in a function.
#include <iostream>
using namespace std;

// This function takes a pointer to an integer as an argument and squares
// the value assocated with it.
void square(int *n) {
    *n = *n * *n;
}

int main() {
    int x;

    cout << "Enter an integer: ";
    cin >> x;

    square(&x);

    cout << "The value has been squared: " << x << endl;

    return 0;
}
