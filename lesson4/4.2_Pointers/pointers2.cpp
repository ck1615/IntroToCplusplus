// Example showing further use of pointers to manipulate the data associated
// with a variable.
#include <iostream>
using namespace std;

int main() {
    int x;
    int *p;

    x = 42;
    p = &x;

    cout << "Value of x: " << x << endl;
    cout << "Pointer p: " << p << endl;
    cout << "Dereferencing p: " << * p << endl;

    x += 1; // x is now 43
    cout << "Value of x: " << x << endl;
    cout << "Dereferencing p: " << * p << endl;

    *p += 1; // x is now 44
    cout << "Value of x: " << x << endl;
    cout << "Dereferencing p: " << * p << endl;

    // THIS CHANGES WHERE p POINTS TO AND MAY CAUSE A SEGMENTATION FAULT!
    // DON'T DO THIS IN YOUR OWN CODES!
    p += 1;
    cout << "Value of x: " << x << endl;
    cout << "Pointer p: " << p << endl;
    cout << "Dereferencing p: " << *p << endl;

    return 0;
}
