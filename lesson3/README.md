Introduction to C++: Lesson 3
=============================

Topics
------

1. [Conditions](#31-conditions)
2. [Loops](#32-loops)

3.1 Conditions
--------------

Conditional statements allow the flow of a program to be changed. Examples of
this are given in the `3.1_Conditions` directory. The most common way this is
done is using an `if` statement as shown in `conditions1.cpp`:

```c++
// Examples of if conditional statements. These test whether a statement is
// true or false and are used to control the flow of a program.
#include <iostream>
using namespace std;

int main()
{
    bool condition1 = true;
    int i = 1;
    int j = 2;

    // This is the basic if statement construction. The whatever is in
    // parentheses evaluates to true, the statements enclosed in braces
    // following it are carried out. Here we are just testing if a bool is
    // true.
    if (condition1) {
        cout << "condition1 is true." << endl;
    }

    // To test equality note that we need to use "==". This is becuase
    // "=" is used for assigning a value to a variable. Be careful as this is
    // a common bug.
    if (i == 1) {
        cout << "i is equal to 1";
        // if statements can be nested.
        if (j == 1) {
            cout << " and j is also equal to 1." << endl;
        } else if (j == 0) {
            cout << " and j is also equal to 0." << endl;
        } else {
            cout << " but j is not equal to 1 or 0." << endl;
        }
    }

    // A '!' before a test indicates logical negation of the result.
    // if condition1 is true then !condition1 is false.
    if (!condition1) {
        cout << "condition1 is false." << endl;
    } else {
        cout << "condition1 is true." << endl;
    }

    // != is ued for inequality
    if (i != 2) {
        cout << "i is not equal to 2." << endl;
    }

    // > and < are for greater than and less than.
    if (i > 0) {
        cout << "i is greater than 0" << endl;
    }

    // >= and <= are for greater than or equal and less than or equal.
    if (i <= 1) {
        cout << "i is less tha or equal to 1." << endl;
    }

    // We can combine expressions also.
    // && is used for logical and.
    if (i == 1 && j == 2) {
        cout << "i is 1 and j is 2." << endl;
    }

    if (i == 1 && j == 0) {
        cout << "i is 1 and j is 0." << endl;
    } else {
        cout << "Either i is not 1 or j is not 0." << endl;
    }

    // || is used for logical or.
    if (i == 1 || j == 0) {
        cout << "i is 1, or j is 0." << endl;
    }

    if (i == 0 || j == 2) {
        cout << "i is 0, or j is 2." << endl;
    }

    // This is what happens if you mistakenly use a single '='.
    if (i = 2) {
        cout << "The value of i is now " << i << endl;
    }

    cout << "Enter an integer: ";
    cin >> i;
    if (i < 10) {
        cout << "i is less than 10." << endl;
    } else if (i > 10) {
        cout << "i is greater than 10." << endl;
    } else {
        cout << "i is equal to 10." << endl;
    }

    return 0;
}
```

It is also possible to use a `switch` statement, which allows code to branch
into several potential options depending on the value of a variable. This
is shown in `conditions2.cpp`:

```c++
// Example of the switch statement.
#include <iostream>
using namespace std;

int main()
{
    int i = 1;
    char c;

    // switch allows you to test a value against several different options.
    switch(i) {
        case 0 :
            cout << "i = 0" << endl;
            cout << "This won't be output." << endl;
            break;
            // The break statement exits the switch statement.
        case 1 :
            cout << "i = 1" << endl;
            break;
        default :
            // The default section is processed if no other case values match.
            cout << "i didn't equal any of the options." << endl;
            break;
    }

    cout << "Please enter y or n and press return: ";
    cin >> c;
    switch(c) {
        // Note switch statements won't work with strings by default as they
        // are not in the basic language (only through the standard library).
        case 'y' :
            cout << "You entered y." << endl;
            break;
        case 'n' :
            cout << "You entered n." << endl;
            break;
        default :
            cout << "You didn't enter y or n!" << endl;
            break;
    }

    return 0;
}
```

Note one can generate code that is functionally the same as that produced
by a switch statement using if statements instead.

### [Exercises](../exercises/README.md#32-conditions)

Please complete the exercises listed in
[section 3.1](../exercises/README.md#32-conditions) of the file
`exercises/README.md`.

3.2 Loops
---------

Loops are used to repeat a section of code until some condition is satisfied.
We'll look at two types of constructions can be used to achieve this: the
`while` loop and the `for` loop. Examples of these are given in
`3.2_Loops/loops.cpp`:

```c++
// Examples of loops.
// These repeatedly perform a set of statements until a condition is satisfied.
#include <iostream>
using namespace std;

int main() {
    int i;
    string ans;

    // The first type of loop is the while loop.

    // We can also use this to ensure input is valid.
    cout << "Enter y or n followed by return: ";
    cin >> ans;
    while (ans != "y" && ans != "n") {
        cout << "You didn't enter y or n. Enter y or n followed by return: ";
        cin >> ans;
    }

    if (ans == "y") {
        cout << "You entered y." << endl;
    } else {
        cout << "You entered n." << endl;
    }

    // Or we can loop over a range of values.
    cout << "while loop from 1 to 10." << endl;
    i = 1;
    while (i <= 10) {
        cout << "i = " << i << endl;
        i = i + 1;
        // we can assign a value to a variable that depends on its previous
        // value. This increments the value of i by 1.
        // Note this is typically written as "++i" or "i++" which is
        // equivalent.
        // Similarly "--i" is equivalent to "i = i - 1".
    }

    // The second type of loop is the for loop. This allows a more direct
    // way to loop over a set integer range.
    cout << "for loop from 1 to 10." << endl;
    for (i = 1; i <= 10; ++i) {
        cout << "i = " << i << endl;
    }

    cout << "for loop in powers of 2." << endl;
    for (i = 1; i <= 256; i = i * 2) {
        cout << "i = " << i << endl;
    }

    return 0;
}
```

### [Exercises](../exercises/README.md#32-loops)

Please complete the exercises listed in
[section 3.2](../exercises/README.md#32-loops) of the file
`exercises/README.md`.

