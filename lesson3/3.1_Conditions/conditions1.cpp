// Examples of if conditional statements. These test whether a statement is
// true or false and are used to control the flow of a program.
#include <iostream>
using namespace std;

int main()
{
    bool condition1 = true;
    int i = 1;
    int j = 2;

    // This is the basic if statement construction. The whatever is in
    // parentheses evaluates to true, the statements enclosed in braces
    // following it are carried out. Here we are just testing if a bool is
    // true.
    if (condition1) {
        cout << "condition1 is true." << endl;
    }

    // To test equality note that we need to use "==". This is becuase
    // "=" is used for assigning a value to a variable. Be careful as this is
    // a common bug.
    if (i == 1) {
        cout << "i is equal to 1";
        // if statements can be nested.
        if (j == 1) {
            cout << " and j is also equal to 1." << endl;
        } else if (j == 0) {
            cout << " and j is also equal to 0." << endl;
        } else {
            cout << " but j is not equal to 1 or 0." << endl;
        }
    }

    // A '!' before a test indicates logical negation of the result.
    // if condition1 is true then !condition1 is false.
    if (!condition1) {
        cout << "condition1 is false." << endl;
    } else {
        cout << "condition1 is true." << endl;
    }

    // != is ued for inequality
    if (i != 2) {
        cout << "i is not equal to 2." << endl;
    }

    // > and < are for greater than and less than.
    if (i > 0) {
        cout << "i is greater than 0" << endl;
    }

    // >= and <= are for greater than or equal and less than or equal.
    if (i <= 1) {
        cout << "i is less tha or equal to 1." << endl;
    }

    // We can combine expressions also.
    // && is used for logical and.
    if (i == 1 && j == 2) {
        cout << "i is 1 and j is 2." << endl;
    }

    if (i == 1 && j == 0) {
        cout << "i is 1 and j is 0." << endl;
    } else {
        cout << "Either i is not 1 or j is not 0." << endl;
    }

    // || is used for logical or.
    if (i == 1 || j == 0) {
        cout << "i is 1, or j is 0." << endl;
    }

    if (i == 0 || j == 2) {
        cout << "i is 0, or j is 2." << endl;
    }

    // This is what happens if you mistakenly use a single '='.
    if (i = 2) {
        cout << "The value of i is now " << i << endl;
    }

    cout << "Enter an integer: ";
    cin >> i;
    if (i < 10) {
        cout << "i is less than 10." << endl;
    } else if (i > 10) {
        cout << "i is greater than 10." << endl;
    } else {
        cout << "i is equal to 10." << endl;
    }

    return 0;
}
