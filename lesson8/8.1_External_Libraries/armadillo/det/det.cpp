// Code to show how to use the armadillo library to calculate the determinant
// of a matrix.
#include <iostream>
#include <armadillo>

using namespace std;
using namespace arma;

int main() {
    mat A;
    // Here we use the armadillo matrix defined type that specifies a matrix
    // of doubles. Note we don't need to specify the dimensions.

    A.load("A.dat", raw_ascii);
    // This will read the values stored in the file A.dat into the matrix A.
    cout << det(A) << endl;
    // Now we output the result of the armadillo "det" function.

    return 0;
}
