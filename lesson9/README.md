Introduction to C++: Lesson 9
=============================

In this class we won't cover any new material. You can continue to work on
the exercises from previous lessons and I'll be available to help you with
any questions or issues you have.

Topics
------

1. [Further Reading](#91-further-reading)

9.1 Further Reading
------------------

Here is a brief list of topics that are beyond the scope of this course, but I
encourage you to research for yourself. Many will be useful as you progress to
developing more advanced codes. You can find more about these topics in
any of the additional resources listed in the
[Course Introduction](../README.md#additional-resources), or by simply
internet searching for the terms listed below.

- stdio library
  - The C input/output library can also be used in C++ and gives you a more
    direct way to control the format of output and input data.
- Function pointers
  - Pointers can also be used to address functions. This can be useful where
    you want a function that takes another functions as an argument.
- Overloading
  - This allows e.g. a function to be specified for arguments of different
    types.
- C++ std::complex
  - There is a complex type available in the modern C++ standard.
- C++ std::array
  - Offers several advantages over the default fixed size array type, such as
    retaining knowledge of their size, and several other useful
    functionalities.
- C++ std::vector
  - Similar to the above this offers many additional functionalities and
    greater flexibility such as resizing and bounds checking.
- C++ templates
  - This allows functions and classes to operate on many different types
    rather than having to rewrite them for each one.
- Header guard
  - This is used to avoid problems of double inclusion, where header files
    include other header files. This is also referred to as an "include guard",
    or "macro guard".
- Recursion
  - This is when a function calls itself. There are many problems where this
    enables quite elegant solutions, but is not that commonly used in codes
    for materials simulations.
- Linked lists
  - This is an ordered set of elements, where each element contains some
    data along with a pointer to the next element, and sometimes also the
    previous element (doubly linked list). It allows much more flexibility
    than an array, at the expense of performance. This can be constructed
    using structures.
- Debuggers
  - Debuggers are used to test and debug  source code, and let you for example,
    watch how the value stored in a variable changes as each step of the source
    is executed.
  - There are many debuggers available for C++. You have the GNU debugger `gdb`
    installed already on your VMs, and it is generally installed by default on
    most Linux systems. This is a command line tool and it takes some time to
    get to know how best to use it.
- IDE
  - This stands for Integraded Development Environment. These provide a more
    comprehensive set of tools for software development than using a text
    editor. Both Emacs and Vim are used as IDEs on Linux, but there are
    also graphical tools available such as Geany and Eclipse.

If you want to see an example of a well-written C++ code actively used in
simulating materials properties, I suggest looking at the source for
Qbox from <http://qboxcode.org>.
